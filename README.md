# OpenJDK Version 8/11 binaries built by AdoptOpenJDK: Docker Image

## References

*	[adoptopenjdk/openjdk8](https://hub.docker.com/r/adoptopenjdk/openjdk8/tags?name=jdk8u-alpine)
*	[adoptopenjdk/openjdk11](https://hub.docker.com/r/adoptopenjdk/openjdk11/tags?name=jdk11u-alpine)
*	[11/jdk/alpine/Dockerfile.hotspot.releases.slim](https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/11/jdk/alpine/Dockerfile.hotspot.releases.slim)
*	[8/jdk/alpine/Dockerfile.hotspot.releases.slim](https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/8/jdk/alpine/Dockerfile.hotspot.releases.slim)

## Build

### The Latest Version by Docker

~~~sh
./fetch-app.sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-adoptopenjdk:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-adoptopenjdk" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).
